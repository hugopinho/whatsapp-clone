package pt.sapo.dynip.hugopinho.whatsappclone.helper;

import android.net.Uri;
import android.util.Log;

import androidx.annotation.NonNull;

import com.bumptech.glide.util.Util;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;

import pt.sapo.dynip.hugopinho.whatsappclone.config.ConfiguracaoFirebase;
import pt.sapo.dynip.hugopinho.whatsappclone.model.Utilizador;

public class UtilizadorFirebase {

    public static String getIdUtilizador(){
        FirebaseAuth utilizador = ConfiguracaoFirebase.getFirebaseAutenticacao();
        String email = utilizador.getCurrentUser().getEmail();
        String identificadorUtilizador = Base64Custom.codificarBase64(email);

        return identificadorUtilizador;
    }

    public static FirebaseUser getUtilizadorAtual(){
        FirebaseAuth utilizador = ConfiguracaoFirebase.getFirebaseAutenticacao();
        return  utilizador.getCurrentUser();
    }

    public static boolean atualizarFotoutilizador(Uri url){

        try{
            FirebaseUser user = getUtilizadorAtual();
            UserProfileChangeRequest profile = new UserProfileChangeRequest.Builder()
                    .setPhotoUri(url)
                    .build();

            user.updateProfile(profile).addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    if(!task.isSuccessful()){
                        Log.d("Perfil","Erro ao atualizar a foto de perfil" );
                    }
                }
            });
            return true;
        }catch (Exception e ){
            e.printStackTrace();
            return false;
        }

    }

    public static boolean atualizarNomeUtilizador(String nome){

        try{
            FirebaseUser user = getUtilizadorAtual();
            UserProfileChangeRequest profile = new UserProfileChangeRequest.Builder()
                    .setDisplayName(nome)
                    .build();

            user.updateProfile(profile).addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    if(!task.isSuccessful()){
                        Log.d("Perfil","Erro ao atualizar a nome de perfil" );
                    }
                }
            });
            return true;
        }catch (Exception e ){
            e.printStackTrace();
            return false;
        }

    }

    public static Utilizador getDadosUtilizadorLogado(){
        FirebaseUser firebaseUser = getUtilizadorAtual();

        Utilizador utilizador = new Utilizador();

        utilizador.setEmail(firebaseUser.getEmail());
        utilizador.setNome(firebaseUser.getDisplayName());

        if(firebaseUser.getPhotoUrl() == null){
            utilizador.setFoto("");
        }else{
            utilizador.setFoto(firebaseUser.getPhotoUrl().toString());
        }

        return utilizador;
    }

}
