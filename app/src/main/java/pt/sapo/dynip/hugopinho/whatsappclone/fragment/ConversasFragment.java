package pt.sapo.dynip.hugopinho.whatsappclone.fragment;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;

import java.util.ArrayList;
import java.util.List;

import pt.sapo.dynip.hugopinho.whatsappclone.R;
import pt.sapo.dynip.hugopinho.whatsappclone.activity.ChatActivity;
import pt.sapo.dynip.hugopinho.whatsappclone.adapter.ConversasAdapter;
import pt.sapo.dynip.hugopinho.whatsappclone.config.ConfiguracaoFirebase;
import pt.sapo.dynip.hugopinho.whatsappclone.helper.RecyclerItemClickListener;
import pt.sapo.dynip.hugopinho.whatsappclone.helper.UtilizadorFirebase;
import pt.sapo.dynip.hugopinho.whatsappclone.model.Conversa;
import pt.sapo.dynip.hugopinho.whatsappclone.model.Utilizador;


public class ConversasFragment extends Fragment {

    private RecyclerView recyclerViewListaConversas;
    private List<Conversa> listaConversas = new ArrayList<>();
    private ConversasAdapter adapter;
    private DatabaseReference databaseReference;
    private  DatabaseReference conversasRef;
    private ChildEventListener childEventListenerConversas;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_conversas, container, false);


        recyclerViewListaConversas = view.findViewById(R.id.recyclerListaConversas);

        //configurar o adaptar
        adapter = new ConversasAdapter(getActivity(), listaConversas);

        //configurar o recyclerview
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerViewListaConversas.setLayoutManager(layoutManager);
        recyclerViewListaConversas.setHasFixedSize(true);
        recyclerViewListaConversas.setAdapter(adapter);

        //configuracao referencias
        String idUtilizador = UtilizadorFirebase.getIdUtilizador();
        databaseReference = ConfiguracaoFirebase.getDatabase();
        conversasRef = databaseReference.child("conversas")
                .child(idUtilizador);


        //configurar evento de click, helper obtido atraves do curso
        recyclerViewListaConversas.addOnItemTouchListener(new RecyclerItemClickListener(
                getActivity(),
                recyclerViewListaConversas,
                new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                       Conversa conversaSelecionada = listaConversas.get(position);

                        //Abrir chat
                        Intent i = new Intent(getActivity(), ChatActivity.class);
                        i.putExtra("chatContacto", conversaSelecionada.getUtilizadorExibicao());
                        startActivity(i);
                    }

                    @Override
                    public void onLongItemClick(View view, int position) {

                    }

                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                    }
                }

        ));

        return view;
    }


    @Override
    public void onStart() {
        super.onStart();
        recuperarConversas();
    }

    @Override
    public void onStop() {
        super.onStop();
        conversasRef.removeEventListener(childEventListenerConversas);
    }


    public void recuperarConversas() {

     childEventListenerConversas = conversasRef.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

                //recuperar conversas
                Conversa conversa = dataSnapshot.getValue(Conversa.class );
                listaConversas.add(conversa);
                adapter.notifyDataSetChanged();

            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }

    public void pesquisarConversas(String texto){

        List<Conversa> listaConversasBusca = new ArrayList<>();

        for(Conversa conversa : listaConversas){
            String nome = conversa.getUtilizadorExibicao().getNome().toLowerCase();
            String ultimaMensagem = conversa.getUtlimaMensagem().toLowerCase();
            if(nome.contains(texto) || ultimaMensagem.contains(texto)){
                listaConversasBusca.add(conversa);
            }
        }

        adapter = new ConversasAdapter(getActivity(), listaConversasBusca);
        recyclerViewListaConversas.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }


    public void recarregarConversas(){
        adapter = new ConversasAdapter(getActivity(), listaConversas);
        recyclerViewListaConversas.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }


}