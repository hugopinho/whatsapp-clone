package pt.sapo.dynip.hugopinho.whatsappclone.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseAuthInvalidUserException;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;
import com.google.firebase.auth.FirebaseAuthWeakPasswordException;
import com.google.firebase.auth.FirebaseUser;

import pt.sapo.dynip.hugopinho.whatsappclone.R;
import pt.sapo.dynip.hugopinho.whatsappclone.config.ConfiguracaoFirebase;
import pt.sapo.dynip.hugopinho.whatsappclone.model.Utilizador;

public class LoginActivity extends AppCompatActivity {

    private EditText campoEmail, campoPassword;
    private FirebaseAuth auth = ConfiguracaoFirebase.getFirebaseAutenticacao();
    private Utilizador utilizador;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        campoEmail = findViewById(R.id.editLoginEmail);
        campoPassword = findViewById(R.id.editLoginPassword);



    }

    public void abrirTelaCadastro(View v){
        Intent intent = new Intent(this, CadastroActivity.class);
        startActivity(intent);
    }


    public void validarAutenticacaoUtilziador(View v){

        String email = campoEmail.getText().toString();
        String senha = campoPassword.getText().toString();

        if(!email.isEmpty()){
            if(!senha.isEmpty()){

                utilizador = new Utilizador();
                utilizador.setEmail(email);
                utilizador.setSenha(senha);
                logarUtilizador(utilizador);
            }else{
                Toast.makeText(LoginActivity.this,"Preencha a password!", Toast.LENGTH_SHORT).show();
            }
        }else{
            Toast.makeText(LoginActivity.this,"Preencha o email!", Toast.LENGTH_SHORT).show();
        }
    }

    private void logarUtilizador(Utilizador utilizador) {
        auth.signInWithEmailAndPassword(utilizador.getEmail(),utilizador.getSenha()).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {

                if(task.isSuccessful()){
                    abrirTelaPrincipal();
                }else{
                    String excecao = "";

                    try {
                        throw task.getException();
                    }catch (FirebaseAuthInvalidUserException e){
                        excecao="Utilizador não está registado!";
                    }catch (FirebaseAuthInvalidCredentialsException e){
                        excecao="Email e senha não correspondem!";
                    }catch (Exception e){
                        e.printStackTrace();
                        excecao="Erro ao logar o utilizador: " + e.getMessage();
                    }
                    Toast.makeText(LoginActivity.this,excecao,Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    public void abrirTelaPrincipal(){
        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
        startActivity(intent);
    }

    @Override
    protected void onStart() {
        super.onStart();
        FirebaseUser utilizadorAtual = auth.getCurrentUser();

        if(utilizadorAtual !=null){
            abrirTelaPrincipal();
        }
    }
}