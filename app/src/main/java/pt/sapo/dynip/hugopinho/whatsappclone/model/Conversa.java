package pt.sapo.dynip.hugopinho.whatsappclone.model;

import com.google.firebase.database.DatabaseReference;

import pt.sapo.dynip.hugopinho.whatsappclone.config.ConfiguracaoFirebase;

public class Conversa {

    private String idRemetente, idDestinatario, utlimaMensagem;
    private Utilizador utilizadorExibicao;


    public Conversa() {
    }

    public String getIdRemetente() {
        return idRemetente;
    }

    public void setIdRemetente(String idRemetente) {
        this.idRemetente = idRemetente;
    }

    public String getIdDestinatario() {
        return idDestinatario;
    }

    public void setIdDestinatario(String idDestinatario) {
        this.idDestinatario = idDestinatario;
    }

    public String getUtlimaMensagem() {
        return utlimaMensagem;
    }

    public void setUtlimaMensagem(String utlimaMensagem) {
        this.utlimaMensagem = utlimaMensagem;
    }

    public Utilizador getUtilizadorExibicao() {
        return utilizadorExibicao;
    }

    public void setUtilizadorExibicao(Utilizador utilizador) {
        this.utilizadorExibicao = utilizador;
    }

    public void salvar(){

        DatabaseReference databaseReference = ConfiguracaoFirebase.getDatabase();
        DatabaseReference conversaRef = databaseReference.child("conversas");

        conversaRef.child(this.getIdRemetente() ).child(this.getIdDestinatario() ).setValue(this);

    }
}
