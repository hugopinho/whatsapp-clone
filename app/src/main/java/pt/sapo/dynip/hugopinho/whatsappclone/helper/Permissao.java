package pt.sapo.dynip.hugopinho.whatsappclone.helper;

import android.app.Activity;
import android.content.pm.PackageManager;
import android.os.Build;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import java.util.ArrayList;
import java.util.List;

public class Permissao {

    public static boolean validarPermissoes(String[] permissoes, Activity activity, int requestCode){

        //validacao necassaria para android maior que 23
        if(Build.VERSION.SDK_INT >=23 ){

            List<String> listaPermissoes = new ArrayList<>();

            /*
            Percorre as permissões passadas, verificando uma a uma se ja tem a permissao dada
             */
            for (String permissao : permissoes) {
              Boolean temPermissaoDada = ContextCompat.checkSelfPermission(activity, permissao) == PackageManager.PERMISSION_GRANTED;
              //se nao tiver permissao concedida, adiciona a lista de permissoes
              if(!temPermissaoDada) {
                  listaPermissoes.add(permissao);
              }

            }

            //Caso a lista estejavazia nao é necessario pedir permissao
            if(listaPermissoes.isEmpty() ) return true;
            //Conversao de lista para array de strings pois e o que é pedido pelo requestPermissions
            String[] novasPermissoes = new String[listaPermissoes.size()];
            listaPermissoes.toArray(novasPermissoes);

            //solicita permissao
            ActivityCompat.requestPermissions(activity,novasPermissoes, requestCode);


        }

        return true;
    }

}
