package pt.sapo.dynip.hugopinho.whatsappclone.fragment;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.renderscript.Sampler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import com.bumptech.glide.util.Util;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import pt.sapo.dynip.hugopinho.whatsappclone.R;
import pt.sapo.dynip.hugopinho.whatsappclone.activity.ChatActivity;
import pt.sapo.dynip.hugopinho.whatsappclone.adapter.ContactosAdapter;
import pt.sapo.dynip.hugopinho.whatsappclone.config.ConfiguracaoFirebase;
import pt.sapo.dynip.hugopinho.whatsappclone.helper.RecyclerItemClickListener;
import pt.sapo.dynip.hugopinho.whatsappclone.helper.UtilizadorFirebase;
import pt.sapo.dynip.hugopinho.whatsappclone.model.Utilizador;


public class ContactosFragment extends Fragment {

    private RecyclerView recyclerViewListaContatos;
    private ContactosAdapter adapter;
    private ArrayList<Utilizador> listaContactos = new ArrayList<>();
    private DatabaseReference utilizadoresRef;
    private ValueEventListener valueEventListenerContactos;
    private FirebaseUser utilizadorAtual;

    public ContactosFragment() {
        // Required empty public constructor
    }



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_contactos, container, false);

        //Configuraçoes iniciais
        utilizadoresRef = ConfiguracaoFirebase.getDatabase().child("utilizadores");
        utilizadorAtual = UtilizadorFirebase.getUtilizadorAtual();
        recyclerViewListaContatos = view.findViewById(R.id.recyclerViewListaContactos);

        //configurar adapter
        adapter = new ContactosAdapter(listaContactos, getActivity());

        //configurar recyclerview
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity() );
        recyclerViewListaContatos.setLayoutManager(layoutManager);
        recyclerViewListaContatos.setHasFixedSize(true);
        recyclerViewListaContatos.setAdapter( adapter );

        //configurar evento de clique no recyclerview
        recyclerViewListaContatos.addOnItemTouchListener(
                new RecyclerItemClickListener(
                        getActivity(),
                        recyclerViewListaContatos,
                        new RecyclerItemClickListener.OnItemClickListener() {
                            @Override
                            public void onItemClick(View view, int position) {

                                Utilizador utilizadorSelecionada = listaContactos.get(position);

                                //Abrir chat
                                Intent i = new Intent(getActivity(), ChatActivity.class);
                                i.putExtra("chatContacto", utilizadorSelecionada);
                                startActivity(i);

                            }

                            @Override
                            public void onLongItemClick(View view, int position) {

                            }

                            @Override
                            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                            }
                        }
                )
        );


        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        //vai buscar todos os contactos a bd do firebase
        recuperarContatos();
    }

    @Override
    public void onStop() {
        super.onStop();
        //remove o listener de contactos
        utilizadoresRef.removeEventListener(valueEventListenerContactos);
    }

    public void recuperarContatos(){

        valueEventListenerContactos =  utilizadoresRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                listaContactos.clear();

                for (DataSnapshot dados: dataSnapshot.getChildren()){

                    String emailUtilizadorLogado = utilizadorAtual.getEmail();
                    Utilizador utilizador = dados.getValue(Utilizador.class);

                    //se o email do utilizador logado for diferente do que vem da bd entao guarda na lista
                    if(!emailUtilizadorLogado.equals(utilizador.getEmail())){
                        listaContactos.add(utilizador);
                    }
                }

                adapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }



}