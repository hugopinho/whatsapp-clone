package pt.sapo.dynip.hugopinho.whatsappclone.activity;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;

import javax.crypto.Cipher;

import de.hdodenhof.circleimageview.CircleImageView;
import pt.sapo.dynip.hugopinho.whatsappclone.R;
import pt.sapo.dynip.hugopinho.whatsappclone.config.ConfiguracaoFirebase;
import pt.sapo.dynip.hugopinho.whatsappclone.helper.Base64Custom;
import pt.sapo.dynip.hugopinho.whatsappclone.helper.Permissao;
import pt.sapo.dynip.hugopinho.whatsappclone.helper.UtilizadorFirebase;
import pt.sapo.dynip.hugopinho.whatsappclone.model.Utilizador;

public class ConfiguracoesActivity extends AppCompatActivity {

    private  String[] permissoesNecessarias = new String[]{
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.CAMERA
    };

    private static final int SELECAO_CAMERA = 100;
    private static final int SELECAO_GALERIA = 200;

    private CircleImageView circleImageViewPerfil;
    private ImageButton imgBtnCamera, imgBtnGallery;
    private StorageReference storageReference;
    private String idUtilizador;
    private EditText editNome;
    private ImageView btnEditNome;
    private Utilizador utilizadorLogado;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_configuracoes);

        //Configuracoes iniciais
        idUtilizador = UtilizadorFirebase.getIdUtilizador();
        storageReference = ConfiguracaoFirebase.getFirebaseStorage();
        utilizadorLogado = UtilizadorFirebase.getDadosUtilizadorLogado();

        //validar permissoes
        Permissao.validarPermissoes(permissoesNecessarias, this, 1);

        Toolbar toolbar = findViewById(R.id.toolbarPrincipal);
        toolbar.setTitle("Configurações");
        setSupportActionBar(toolbar);

        imgBtnCamera = findViewById(R.id.imageButtonCamera);
        imgBtnGallery = findViewById(R.id.imageButtonGallery);
        circleImageViewPerfil = findViewById(R.id.circleImageViewFotoPerfil);
        editNome = findViewById(R.id.editTextTextPersonName);
        btnEditNome = findViewById(R.id.btnEditNome);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //Recuperar dados do utilizador
        FirebaseUser utilizador = UtilizadorFirebase.getUtilizadorAtual();
        Uri url = utilizador.getPhotoUrl();

        if( url != null){
            Glide.with(ConfiguracoesActivity.this)
            .load(url)
            .into(circleImageViewPerfil);
        }else {
            circleImageViewPerfil.setImageResource(R.drawable.padrao);
        }


        editNome.setText(utilizador.getDisplayName());

        imgBtnCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                if(intent.resolveActivity(getPackageManager()) != null ){
                    startActivityForResult(intent, SELECAO_CAMERA);
                }

            }
        });

        imgBtnGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                if(intent.resolveActivity(getPackageManager()) != null ){
                    startActivityForResult(intent, SELECAO_GALERIA);
                }
            }
        });

        btnEditNome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String nome = editNome.getText().toString();
                boolean retorno = UtilizadorFirebase.atualizarNomeUtilizador(nome);


                if(retorno){

                    utilizadorLogado.setNome(nome);
                    utilizadorLogado.atualizar();


                    Toast.makeText(ConfiguracoesActivity.this,
                            "Nome atualizado com sucesso!",
                            Toast.LENGTH_SHORT).show();
                }

            }
        });
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        for(int permissaoResultado : grantResults) {
            if(permissaoResultado == PackageManager.PERMISSION_DENIED){
                alertaValidacaoPermissao();
            }
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(resultCode == RESULT_OK){

            Bitmap imagem = null;

            try {
                switch ( requestCode){
                    case SELECAO_CAMERA:
                        imagem = (Bitmap) data.getExtras().get("data");
                        break;
                    case SELECAO_GALERIA:
                        Uri localImagemSelecionada = data.getData();
                        imagem = MediaStore.Images.Media.getBitmap(getContentResolver(), localImagemSelecionada);
                        break;
                }

                if( imagem != null){

                    circleImageViewPerfil.setImageBitmap( imagem );

                    //Recuperar dados da imagem para firebase
                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    imagem.compress(Bitmap.CompressFormat.JPEG, 70,baos );
                    byte[] dadosImagem = baos.toByteArray();

                    //Salvar imagem no firebase
                    final StorageReference imageRef = storageReference.child("imagens")
                            .child("perfil")
                            .child(idUtilizador)
                            .child("perfil.jpeg");

                    UploadTask uploadTask = imageRef.putBytes( dadosImagem);
                    uploadTask.addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Toast.makeText(ConfiguracoesActivity.this,
                                    "Erro ao fazer upload da imagem",
                                    Toast.LENGTH_SHORT).show();
                        }
                    }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            Toast.makeText(ConfiguracoesActivity.this,
                                    "Sucesso ao fazer upload da imagem",
                                    Toast.LENGTH_SHORT).show();

                            //Recuperar url de imagem
                            imageRef.getDownloadUrl().addOnCompleteListener(new OnCompleteListener<Uri>() {
                                @Override
                                public void onComplete(@NonNull Task<Uri> task) {
                                  Uri url = task.getResult();
                                  
                                  atualizaFotoUtilizador(url);
                                  
                                }

                            });

                        }
                    });
                }

            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    private void atualizaFotoUtilizador(Uri url) {
        boolean retorno = UtilizadorFirebase.atualizarFotoutilizador(url);

        if(retorno){
            utilizadorLogado.setFoto(url.toString());
            utilizadorLogado.atualizar();

            Toast.makeText(ConfiguracoesActivity.this,
                    "A sua foto foi alterada!",Toast.LENGTH_SHORT).show();
        }

    }

    private  void alertaValidacaoPermissao(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Permissões Negadas");
        builder.setMessage("Para utilizar o app é necesário aceitas as permissões");
        builder.setCancelable(false);
        builder.setPositiveButton("Confirmar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }
}