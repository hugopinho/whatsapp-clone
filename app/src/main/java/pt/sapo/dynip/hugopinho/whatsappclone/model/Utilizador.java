package pt.sapo.dynip.hugopinho.whatsappclone.model;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.Exclude;
import com.google.firebase.database.FirebaseDatabase;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import pt.sapo.dynip.hugopinho.whatsappclone.config.ConfiguracaoFirebase;
import pt.sapo.dynip.hugopinho.whatsappclone.helper.UtilizadorFirebase;

public class Utilizador implements Serializable {

    private String nome, email, senha, id, foto;

    public Utilizador() {
    }

    public void salvar(){
        DatabaseReference firebaseRef = ConfiguracaoFirebase.getDatabase();
        DatabaseReference utilizadores = firebaseRef.child("utilizadores").child(getId());
        //salva objeto inteiro no firebase
        utilizadores.setValue(this);
    }

    public void atualizar(){
        String idUtilizador = UtilizadorFirebase.getIdUtilizador();
        DatabaseReference database = ConfiguracaoFirebase.getDatabase();
        DatabaseReference utilizadorRef =  database.child("utilizadores")
                .child(idUtilizador);

        Map<String, Object> valoresUtilizador = converterParaMap();

        //Update de firebase pede um map e nao object como o setvalue, tem de ser update para nao subescrever sobre valores antigos
        utilizadorRef.updateChildren(valoresUtilizador);
    }


    @Exclude
    public Map<String, Object> converterParaMap(){
        HashMap<String, Object> utilizadorMap = new HashMap<>();
        utilizadorMap.put("email", getEmail());
        utilizadorMap.put("nome", getNome());
        utilizadorMap.put("foto", getFoto());

        return utilizadorMap;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Exclude
    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    @Exclude
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
