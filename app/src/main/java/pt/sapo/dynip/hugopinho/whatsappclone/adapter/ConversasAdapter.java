package pt.sapo.dynip.hugopinho.whatsappclone.adapter;

import android.content.Context;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import pt.sapo.dynip.hugopinho.whatsappclone.R;
import pt.sapo.dynip.hugopinho.whatsappclone.model.Conversa;

public class ConversasAdapter extends RecyclerView.Adapter<ConversasAdapter.MyViewHolder> {

    private List<Conversa> conversas;
    private Context context;

    public ConversasAdapter(Context ctx,List<Conversa> conversasLista) {
        this.context = ctx;
        this.conversas = conversasLista;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemLista = LayoutInflater.from(parent.getContext() ).inflate(R.layout.adapter_contactos, parent, false);
       return new MyViewHolder(itemLista);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        Conversa conversa = conversas.get(position);

        holder.utlimaMensagem.setText(conversa.getUtlimaMensagem());
        holder.nome.setText(conversa.getUtilizadorExibicao().getNome());

        if(conversa.getUtilizadorExibicao().getFoto() != null){
            Uri uri = Uri.parse(conversa.getUtilizadorExibicao().getFoto());
            Glide.with(context ).load(uri).into(holder.fotoContacto);
        }else {
            holder.fotoContacto.setImageResource(R.drawable.padrao);
        }

    }

    @Override
    public int getItemCount() {
        return conversas.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        CircleImageView fotoContacto;
        TextView nome, utlimaMensagem;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            fotoContacto = itemView.findViewById(R.id.imageViewFotoContacto);
            nome = itemView.findViewById(R.id.textViewNome);
            utlimaMensagem = itemView.findViewById(R.id.textViewEmail);
        }
    }

}
