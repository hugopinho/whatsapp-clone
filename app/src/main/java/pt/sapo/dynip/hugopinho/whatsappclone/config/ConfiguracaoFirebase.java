package pt.sapo.dynip.hugopinho.whatsappclone.config;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

public class ConfiguracaoFirebase {

    private static DatabaseReference database;
    public static FirebaseAuth auth;
    public static StorageReference storage;

    //retornar instancia do FirebaseDatabade
    public static DatabaseReference getDatabase(){
        if( database == null){
            database = FirebaseDatabase.getInstance().getReference();
        }

        return database;
    }



    //retornar instancia do FirebaseAuth
    public static FirebaseAuth getFirebaseAutenticacao(){
        if( auth == null){
            auth = FirebaseAuth.getInstance();
        }

        return auth;
    }

    //retornar instancia do FirebaseStorage
    public static StorageReference getFirebaseStorage(){
        if( storage == null){
            storage = FirebaseStorage.getInstance().getReference();
        }

        return storage;
    }

}
