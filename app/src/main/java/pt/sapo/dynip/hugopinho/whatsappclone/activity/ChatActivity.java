package pt.sapo.dynip.hugopinho.whatsappclone.activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;

import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Adapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import de.hdodenhof.circleimageview.CircleImageView;
import pt.sapo.dynip.hugopinho.whatsappclone.R;
import pt.sapo.dynip.hugopinho.whatsappclone.adapter.MensagensAdapter;
import pt.sapo.dynip.hugopinho.whatsappclone.config.ConfiguracaoFirebase;
import pt.sapo.dynip.hugopinho.whatsappclone.helper.Base64Custom;
import pt.sapo.dynip.hugopinho.whatsappclone.helper.UtilizadorFirebase;
import pt.sapo.dynip.hugopinho.whatsappclone.model.Conversa;
import pt.sapo.dynip.hugopinho.whatsappclone.model.Mensagem;
import pt.sapo.dynip.hugopinho.whatsappclone.model.Utilizador;

public class ChatActivity extends AppCompatActivity {

    private TextView textNome;
    private CircleImageView circleImageViewFoto;
    private Utilizador utilizadorDestino;
    private EditText editMensagem;
    private ImageView imageCamera;

    private String idUtilizadorRemetente, idUtilizadorDestinatario;

    private RecyclerView recyclerViewMensagens;
    private MensagensAdapter adapter;
    private List<Mensagem> listaMensagens = new ArrayList<>();

    private DatabaseReference databaseReference;
    private DatabaseReference mensagensRef;
    private StorageReference storage;
    private ChildEventListener childEventListenerMensagens;

    private static final int SELECAO_CAMERA = 100;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        //configuracoes toolbar
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //configuracoes iniciais
        textNome = findViewById(R.id.textNomeChat);
        circleImageViewFoto = findViewById(R.id.circleImageFotoChat);
        editMensagem = findViewById(R.id.editMensagem);
        recyclerViewMensagens = findViewById(R.id.recyclerViewMensagens);
        imageCamera = findViewById(R.id.imageCamera);

        //recuperar dados do utilizador remetente
        idUtilizadorRemetente = UtilizadorFirebase.getIdUtilizador();
        storage = ConfiguracaoFirebase.getFirebaseStorage();

        //Recuperar dados do utilizador destinatario
        Bundle bundle = getIntent().getExtras();

        if(bundle != null){

            utilizadorDestino = (Utilizador) bundle.getSerializable("chatContacto");
            textNome.setText(utilizadorDestino.getNome());

            if(utilizadorDestino.getFoto() != null){
                Uri uri = Uri.parse(utilizadorDestino.getFoto());
                Glide.with(ChatActivity.this).load(uri).into(circleImageViewFoto);
            }else {
                circleImageViewFoto.setImageResource(R.drawable.padrao);
            }

            idUtilizadorDestinatario = Base64Custom.codificarBase64(utilizadorDestino.getEmail());

        }


        //Configuracao Adapter
        adapter = new MensagensAdapter(listaMensagens,getApplicationContext());

        //Configuracao Recyclerview
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(ChatActivity.this);
        recyclerViewMensagens.setLayoutManager( layoutManager);
        recyclerViewMensagens.setHasFixedSize(true);
        recyclerViewMensagens.setAdapter(adapter);

        //recuperar mensagens
        databaseReference = ConfiguracaoFirebase.getDatabase();
        mensagensRef =  databaseReference.child("mensagens")
                .child(idUtilizadorRemetente)
                .child(idUtilizadorDestinatario);


        imageCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                if(intent.resolveActivity(getPackageManager()) != null ){
                    startActivityForResult(intent, SELECAO_CAMERA);
                }
            }
        });

    }

    public void enviarMensagem(View view){
        String textoMensagem = editMensagem.getText().toString();

        if(!textoMensagem.isEmpty()){
            Mensagem msg = new Mensagem();
            msg.setIdUtilizador( idUtilizadorRemetente );
            msg.setMensagem(textoMensagem);

            //Salvar mensagem para remetente
            salvarMensagem(idUtilizadorRemetente,idUtilizadorDestinatario,msg);

            //Salvar mensagem para destinatario
            salvarMensagem(idUtilizadorDestinatario,idUtilizadorRemetente,msg);


            //Salvar Conversa
            salvarConversa(msg);

        }else{
            Toast.makeText(ChatActivity.this,
                    "Digite uma mensagem",
                    Toast.LENGTH_SHORT).show();
        }

    }

    private void salvarConversa(Mensagem msg) {

        Conversa conversaRemetente = new Conversa();

        conversaRemetente.setIdRemetente(idUtilizadorRemetente);
        conversaRemetente.setIdDestinatario(idUtilizadorDestinatario);
        conversaRemetente.setUtlimaMensagem(msg.getMensagem());
        //vem do bundle
        conversaRemetente.setUtilizadorExibicao(utilizadorDestino);

        conversaRemetente.salvar();
    }

    private void salvarMensagem(String idRemetente, String idDestinatario, Mensagem msg){
         databaseReference = ConfiguracaoFirebase.getDatabase();
         mensagensRef = databaseReference.child("mensagens");

        //push cria id unico de firebase para nao substituir mensagens
        mensagensRef.child(idRemetente)
                .child(idDestinatario)
                .push()
                .setValue(msg);

        //limpar texto
        editMensagem.setText("");
    }


    @Override
    protected void onStart() {
        super.onStart();
        recuperarMensagens();
    }

    @Override
    protected void onStop() {
        super.onStop();
        mensagensRef.removeEventListener(childEventListenerMensagens);
    }

    private void recuperarMensagens(){

        childEventListenerMensagens = mensagensRef.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                Mensagem mensagem = dataSnapshot.getValue(Mensagem.class);
                listaMensagens.add(mensagem);
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(resultCode == RESULT_OK){

            Bitmap imagem = null;

            try {
                switch ( requestCode){
                    case SELECAO_CAMERA:
                        imagem = (Bitmap) data.getExtras().get("data");
                        break;

                }

                if( imagem != null){

                    //Recuperar dados da imagem para firebase
                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    imagem.compress(Bitmap.CompressFormat.JPEG, 70,baos );
                    byte[] dadosImagem = baos.toByteArray();

                    //Criar nome da imagem
                    String nomeImagem = UUID.randomUUID().toString();

                    //Salvar imagem no firebase
                    final StorageReference imageRef = storage.child("imagens")
                            .child("fotos")
                            .child(idUtilizadorRemetente)
                            .child(nomeImagem + ".jpeg");

                    UploadTask uploadTask = imageRef.putBytes( dadosImagem);
                    uploadTask.addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Toast.makeText(ChatActivity.this,
                                    "Erro ao fazer upload da imagem",
                                    Toast.LENGTH_SHORT).show();
                        }
                    }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            Toast.makeText(ChatActivity.this,
                                    "Sucesso ao fazer upload da imagem",
                                    Toast.LENGTH_SHORT).show();

                            //Recuperar url de imagem
                            imageRef.getDownloadUrl().addOnCompleteListener(new OnCompleteListener<Uri>() {
                                @Override
                                public void onComplete(@NonNull Task<Uri> task) {
                                    Uri url = task.getResult();

                                    Mensagem mensagem = new Mensagem();
                                    mensagem.setIdUtilizador(idUtilizadorRemetente);
                                    mensagem.setMensagem("imagem.jpeg");
                                    mensagem.setImagem(url.toString());


                                    //salvar mensagem para rememente
                                    salvarMensagem(idUtilizadorRemetente,idUtilizadorDestinatario,mensagem);

                                    //salvar mensagem para destinatario
                                    salvarMensagem(idUtilizadorDestinatario,idUtilizadorRemetente,mensagem);

                                }

                            });

                        }
                    });
                }

            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }


}