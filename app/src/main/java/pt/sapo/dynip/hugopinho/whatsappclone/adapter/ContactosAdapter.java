package pt.sapo.dynip.hugopinho.whatsappclone.adapter;

import android.content.Context;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.sql.ClientInfoStatus;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import pt.sapo.dynip.hugopinho.whatsappclone.R;
import pt.sapo.dynip.hugopinho.whatsappclone.model.Utilizador;

public class ContactosAdapter extends RecyclerView.Adapter<ContactosAdapter.MyViewHolder> {

    private List<Utilizador> contactos;
    private Context context;

    public ContactosAdapter(List<Utilizador> listContactos, Context c) {
        this.context = c;
        this.contactos = listContactos;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemList = LayoutInflater.from(parent.getContext() ).inflate(R.layout.adapter_contactos, parent, false);
        return new MyViewHolder(itemList);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        Utilizador utilizador = contactos.get(position);
        holder.nome.setText(utilizador.getNome());
        holder.email.setText(utilizador.getEmail());


        if(utilizador.getFoto() != null){
            Uri uri = Uri.parse(utilizador.getFoto());
            Glide.with(context ).load(uri).into(holder.fotoContacto);
        }else {
            holder.fotoContacto.setImageResource(R.drawable.padrao);
        }

    }

    @Override
    public int getItemCount() {
        return contactos.size();
    }



    //Inner class ViewHoler que é o extends do adapter
    public class  MyViewHolder extends RecyclerView.ViewHolder {

        CircleImageView fotoContacto;
        TextView nome, email;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            fotoContacto = itemView.findViewById(R.id.imageViewFotoContacto);
            nome = itemView.findViewById(R.id.textViewNome);
            email = itemView.findViewById(R.id.textViewEmail);
        }

    }



}
