package pt.sapo.dynip.hugopinho.whatsappclone.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;
import com.google.firebase.auth.FirebaseAuthWeakPasswordException;

import pt.sapo.dynip.hugopinho.whatsappclone.R;
import pt.sapo.dynip.hugopinho.whatsappclone.config.ConfiguracaoFirebase;
import pt.sapo.dynip.hugopinho.whatsappclone.helper.Base64Custom;
import pt.sapo.dynip.hugopinho.whatsappclone.helper.UtilizadorFirebase;
import pt.sapo.dynip.hugopinho.whatsappclone.model.Utilizador;

public class CadastroActivity extends AppCompatActivity {

    private EditText campoNome, campoEmail, campoPassword;
    private Button btnCadastrar;
    private FirebaseAuth auth;
    private Utilizador utilizador;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastro);

        campoNome = findViewById(R.id.editNome);
        campoEmail = findViewById(R.id.editLoginEmail);
        campoPassword = findViewById(R.id.editLoginPassword);
        btnCadastrar = findViewById(R.id.btnRegistar);


    }



    public void validarCadastroUtilizador(View view){

        //Recuperar textos dos campos
        String textoNome = campoNome.getText().toString();
        String textoEmail = campoEmail.getText().toString();
        String textoPassword = campoPassword.getText().toString();

        Log.v("TAG", textoNome + " " + textoEmail + " " + textoPassword);

        if(!textoNome.isEmpty()){
            if(!textoEmail.isEmpty()){
                if(!textoPassword.isEmpty()){
                    utilizador = new Utilizador();
                    utilizador.setNome(textoNome);
                    utilizador.setEmail(textoEmail.toLowerCase());
                    utilizador.setSenha(textoPassword);

                    cadastrarUtilizador(utilizador);
                }else{
                    Toast.makeText(CadastroActivity.this,"Preencha a Password!",Toast.LENGTH_SHORT).show();
                }
            }else{
                Toast.makeText(CadastroActivity.this,"Preencha o Email!",Toast.LENGTH_SHORT).show();
            }
        }else{
            Toast.makeText(CadastroActivity.this,"Preencha o nome!",Toast.LENGTH_SHORT).show();
        }


    }

    private void cadastrarUtilizador(final Utilizador utilizador) {

        auth = ConfiguracaoFirebase.getFirebaseAutenticacao();
        auth.createUserWithEmailAndPassword(
        utilizador.getEmail(), utilizador.getSenha()
        ).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {

                if(task.isSuccessful()){

                    Toast.makeText(CadastroActivity.this,"Conta registada com sucesso!",Toast.LENGTH_SHORT).show();

                    UtilizadorFirebase.atualizarNomeUtilizador(utilizador.getNome());

                    finish();

                    try {

                        String identificadorUtilizador = Base64Custom.codificarBase64(utilizador.getEmail());
                        utilizador.setId(identificadorUtilizador);
                        utilizador.salvar();

                    }catch (Exception e ){
                        e.printStackTrace();
                    }

                }else {
                    String excecao = "";

                    try {
                        throw task.getException();
                    }catch (FirebaseAuthWeakPasswordException e){
                       excecao="Digite uma password mais forte!";
                    }catch (FirebaseAuthInvalidCredentialsException e){
                       excecao="Por favor, insira um email válido!";
                    }catch (FirebaseAuthUserCollisionException e){
                       excecao="Esta conta já está criada!";
                    }catch (Exception e){
                        e.printStackTrace();
                        excecao="Erro ao registar utilizador: " + e.getMessage();
                    }
                    Toast.makeText(CadastroActivity.this,excecao,Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

}