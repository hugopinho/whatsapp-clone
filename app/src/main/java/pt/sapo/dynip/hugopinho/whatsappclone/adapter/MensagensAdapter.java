package pt.sapo.dynip.hugopinho.whatsappclone.adapter;

import android.content.Context;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

import pt.sapo.dynip.hugopinho.whatsappclone.R;
import pt.sapo.dynip.hugopinho.whatsappclone.helper.UtilizadorFirebase;
import pt.sapo.dynip.hugopinho.whatsappclone.model.Mensagem;

public class MensagensAdapter extends RecyclerView.Adapter<MensagensAdapter.MyViewHolder> {

    private List<Mensagem> listaMensagens = new ArrayList<>();
    private Context context;

    private static final int TIPO_REMETENTE = 0 ;
    private static final int TIPO_DESTINATARIO = 1 ;


    public MensagensAdapter(List<Mensagem> lista, Context c) {
        this.listaMensagens = lista;
        this.context = c;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View item  = null ;
        if(viewType == TIPO_REMETENTE){

            item = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_mensagem_remetente,parent,false);

        }else if (viewType == TIPO_DESTINATARIO){
            item = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_mensagem_destinatario,parent,false);
        }

        return new MyViewHolder(item);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        Mensagem mensagem = listaMensagens.get(position);
        String msg = mensagem.getMensagem();
        String imagem = mensagem.getImagem();

        if(imagem != null){
            Uri url = Uri.parse(imagem);
            Glide.with(context).load(url).into(holder.imagemFoto);

            holder.mensagem.setVisibility(View.GONE);
        }else{
            holder.mensagem.setText(msg);

            holder.imagemFoto.setVisibility(View.GONE);

        }


    }

    @Override
    public int getItemCount() {
        return listaMensagens.size();
    }


    //Faz verificacao do tipo de utilizador e passa para funcao onCreateViewHolder na variavel viewType
    @Override
    public int getItemViewType(int position) {

        Mensagem mensagem = listaMensagens.get(position);

        String idUtilizador = UtilizadorFirebase.getIdUtilizador();

        //se id de utilizador logado = ao utilizador que enviou a mensagem
        if(idUtilizador.equals(mensagem.getIdUtilizador())) {
            return TIPO_REMETENTE;
        }

        return TIPO_DESTINATARIO;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{

        TextView mensagem;
        ImageView imagemFoto;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            mensagem = itemView.findViewById(R.id.textMensagemTexto);
            imagemFoto = itemView.findViewById(R.id.imageMensagemFoto);
        }

    }
}
