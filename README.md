# WhatsApp Clone

Esta aplicação é um clone **WhatsApp**. O tema diferente da aplicação WhatsApp para diferenciar da mesma. 

A aplicação tem como principal objectivo o envio de mensagens entre utilizadores registados.

# Recursos

- **Android Studio**
- **Firebase** 
  - Storage
  - Auth
  - Realtime Database
  
- **Biblioteca CircleImage** 
   - https://github.com/hdodenhof/CircleImageView
 - **Biblioteca SmarTablayout**
   - https://github.com/ogaclejapan/SmartTabLayout

